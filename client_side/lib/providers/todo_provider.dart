 import 'dart:convert';

import 'package:http/http.dart' as http;
 import 'package:flutter/material.dart';

 class ProductProvider extends ChangeNotifier{

   final httpClient = http.Client();

   var productData = [];

   Map<String,String> customHeaders = {
     "Accept" : "application/json",
     "Content-Type" : "application/json;charset=UTF-8"
   };
   //Get reauest 

   Future fetchData() async{

     final Uri restAPIURL = Uri.parse("https://loique-todoapp.herokuapp.com/product");

     http.Response response = await httpClient.get(restAPIURL);

     final Map parsedData = await json.decode(response.body.toString());

     productData = parsedData['result'];
     print(productData);
   }

   // POST REQUEST

   Future addProduct(Map<String,String> body) async {

     final addProductUrl = Uri.parse("https://loique-todoapp.herokuapp.com/product");

     http.Response response = await httpClient.post(
       addProductUrl,
       headers: customHeaders,
       body: json.encode(body)
     );

     return response.body;
   }
 }