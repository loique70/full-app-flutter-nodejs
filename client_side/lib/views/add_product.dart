import 'package:client_side/providers/todo_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

final productNameController = TextEditingController();
final productPriceController = TextEditingController();

addProductWidget(BuildContext context){
  return showModalBottomSheet(context: context, builder:(context){
    return Container(
      height: 300,
      width: 400,
      child: Column(
        children: [
          TextField(
            controller:productNameController ,
            decoration: const InputDecoration(
              hintText: 'Add product name',
            ),
          ),

          TextField(
            controller: productPriceController,
            decoration: const InputDecoration(
              hintText: "Add product price"
            ),
          ),

          ElevatedButton(
            onPressed: (){
              if(productNameController.text.isNotEmpty){
                Provider.of<ProductProvider>(context, listen: false)
                .addProduct({
                  "name": productNameController.text,
                  "price": productPriceController.text
                });
              }
            },

            child: const Text("Submit"),
          )
        ],
      ),
    );
  });
}