import 'package:client_side/providers/todo_provider.dart';
import 'package:client_side/views/home_page.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApp());
}


class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      // ignore: sort_child_properties_last
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blueGrey
        ),
        home: HomePage(),
    ),

    providers: [
      ChangeNotifierProvider(create: (_) => ProductProvider())
    ],
    ); 
  }
}