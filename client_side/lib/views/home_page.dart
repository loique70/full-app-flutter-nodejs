import 'package:client_side/providers/todo_provider.dart';
import 'package:client_side/views/add_product.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    Provider.of<ProductProvider>(context,listen: false).fetchData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("Todo App"),
      ),

      body: Container(
        child: Consumer<ProductProvider>(
          builder: (context,model,_) => FutureBuilder(
            future: model.fetchData(),
            builder: (context,snapshot) => ListView.builder(
              itemCount: model.productData.length,
              itemBuilder: (context,int index){
                return ListTile(
                  title: Text(model.productData[index]['name'].toString()),
                  subtitle: Text(model.productData[index]['price'].toString()),
                ); 
              },
            ),
          ),
        ),
      ),

      floatingActionButton: FloatingActionButton(
        onPressed: () => addProductWidget(context),
        child:const Icon(Icons.add),
      ),
    );
  }
}